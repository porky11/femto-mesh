use data_stream::{
    FromStream, ToStream, collections::SizeSettings, default_settings::PortableSettings,
    from_stream, to_stream,
};
use ga2::Vector;

use std::{
    fs::File,
    io::{Read, Result, Write},
    path::Path,
};

/// An animation frame represented by a list of points and the duration.
#[derive(Clone)]
pub struct AnimationFrame {
    /// The list of points of the frame.
    pub points: Vec<Vector<f32>>,
    duration: f32,
}

impl AnimationFrame {
    /// Creates a new animation frame by specifying the `points` of the frame and its `duration`.
    pub fn new(points: Vec<Vector<f32>>, duration: f32) -> Self {
        Self { points, duration }
    }
}

impl<S: SizeSettings> ToStream<S> for AnimationFrame {
    fn to_stream<W: Write>(&self, stream: &mut W) -> Result<()> {
        to_stream::<S, _, _>(&self.points, stream)?;
        to_stream::<S, _, _>(&self.duration, stream)
    }
}

impl<S: SizeSettings> FromStream<S> for AnimationFrame {
    fn from_stream<R: Read>(stream: &mut R) -> Result<Self> {
        Ok(Self {
            points: from_stream::<S, _, _>(stream)?,
            duration: from_stream::<S, _, _>(stream)?,
        })
    }
}

/// A complete frame based animation.
#[derive(Clone)]
pub struct Animation {
    /// The frames of the animation.
    pub frames: Vec<AnimationFrame>,
}

impl Animation {
    /// Creates a new animation from a list of `frames`.
    pub fn new(frames: Vec<AnimationFrame>) -> Self {
        Self { frames }
    }

    /// Sets some `points` to the animation at a specific `state` in the animation.
    pub fn set_shape(&self, points: &mut [Vector<f32>], state: &AnimationState) {
        let next_index = if state.index + 1 == self.frames.len() {
            0
        } else {
            state.index + 1
        };
        let first_frame = &self.frames[state.index];
        let ratio = state.time / first_frame.duration;

        let first_shape = &first_frame.points;
        let next_shape = &self.frames[next_index].points;

        for (point, (first, next)) in points
            .iter_mut()
            .zip(first_shape.iter().zip(next_shape.iter()))
        {
            point.x = first.x * (1.0 - ratio) + next.x * ratio;
            point.y = first.y * (1.0 - ratio) + next.y * ratio;
        }
    }

    /// Saves the animation to a file.
    pub fn save(&self, path: &Path) -> Result<()> {
        let mut file = File::create(path)?;

        to_stream::<PortableSettings, _, _>(self, &mut file)
    }

    /// Loads a new animation from a file.
    pub fn load(path: &Path) -> Result<Self> {
        let mut file = File::open(path)?;

        from_stream::<PortableSettings, _, _>(&mut file)
    }
}

impl<S: SizeSettings> ToStream<S> for Animation {
    fn to_stream<W: Write>(&self, stream: &mut W) -> Result<()> {
        to_stream::<S, _, _>(&self.frames, stream)
    }
}

impl<S: SizeSettings> FromStream<S> for Animation {
    fn from_stream<R: Read>(stream: &mut R) -> Result<Self> {
        Ok(Self {
            frames: from_stream::<S, _, _>(stream)?,
        })
    }
}

/// Represents the state of the animation.
#[derive(Default)]
pub struct AnimationState {
    time: f32,
    /// The curent frame of the animation.
    pub index: usize,
}

impl AnimationState {
    /// Updates the animation frame by the specified timestep.
    pub fn update(&mut self, timestep: f32, animation: &Animation) {
        self.time += timestep;
        while self.time >= animation.frames[self.index].duration {
            self.time -= animation.frames[self.index].duration;
            self.index += 1;
            if self.index == animation.frames.len() {
                self.index = 0;
            }
        }
    }

    /// Skips the animation until the next frame.
    pub fn next_frame(&mut self, animation: &Animation) {
        self.time = 0.0;
        self.index += 1;
        if self.index == animation.frames.len() {
            self.index = 0;
        }
    }
}
