use crate::style::{FillStyle, StrokeStyle, Style};

use data_stream::{
    FromStream, ToStream, default_settings::PortableSettings, from_stream, numbers::EndianSettings,
    to_stream,
};
use femtovg::{Canvas, Path, Renderer};
use ga2::Vector;

use std::io::{Error, ErrorKind, Read, Result, Write};

#[derive(Copy, Clone, Debug)]
enum IndexedVerb {
    MoveTo(usize),
    LineTo(usize),
    QuadTo(usize, usize),
    BezierTo(usize, usize, usize),
    Close,
}

impl ToStream<PortableSettings> for IndexedVerb {
    fn to_stream<W: Write>(&self, stream: &mut W) -> Result<()> {
        use IndexedVerb::*;
        match self {
            MoveTo(index) => {
                to_stream::<PortableSettings, _, _>(&0u8, stream)?;
                to_stream::<PortableSettings, _, _>(index, stream)
            }
            LineTo(index) => {
                to_stream::<PortableSettings, _, _>(&1u8, stream)?;
                to_stream::<PortableSettings, _, _>(index, stream)
            }
            QuadTo(index1, index2) => {
                to_stream::<PortableSettings, _, _>(&2u8, stream)?;
                to_stream::<PortableSettings, _, _>(index1, stream)?;
                to_stream::<PortableSettings, _, _>(index2, stream)
            }
            BezierTo(index1, index2, index3) => {
                to_stream::<PortableSettings, _, _>(&3u8, stream)?;
                to_stream::<PortableSettings, _, _>(index1, stream)?;
                to_stream::<PortableSettings, _, _>(index2, stream)?;
                to_stream::<PortableSettings, _, _>(index3, stream)
            }
            Close => to_stream::<PortableSettings, _, _>(&4u8, stream),
        }
    }
}

impl FromStream<PortableSettings> for IndexedVerb {
    fn from_stream<R: Read>(stream: &mut R) -> Result<Self> {
        let verb_kind: u8 = from_stream::<PortableSettings, _, _>(stream)?;

        use IndexedVerb::*;
        Ok(match verb_kind {
            0 => MoveTo(from_stream::<PortableSettings, _, _>(stream)?),
            1 => LineTo(from_stream::<PortableSettings, _, _>(stream)?),
            2 => QuadTo(
                from_stream::<PortableSettings, _, _>(stream)?,
                from_stream::<PortableSettings, _, _>(stream)?,
            ),
            3 => BezierTo(
                from_stream::<PortableSettings, _, _>(stream)?,
                from_stream::<PortableSettings, _, _>(stream)?,
                from_stream::<PortableSettings, _, _>(stream)?,
            ),
            4 => Close,
            _ => {
                return Err(Error::new(
                    ErrorKind::InvalidData,
                    "Trying to read invalid enum variant",
                ));
            }
        })
    }
}

/// An indexed path.
///
/// Used to represent different vector graphics elements using indices to a point list.
/// Each path also specifies its own render style.
#[derive(Clone)]
pub struct IndexedPath {
    verbs: Vec<IndexedVerb>,
    style: Style,
}

impl IndexedPath {
    /// Creates a stroke only path.
    pub fn stroke(style: StrokeStyle) -> Self {
        Self {
            verbs: Vec::new(),
            style: Style::Stroke(style),
        }
    }

    /// Creates a fill only path.
    pub fn fill(style: FillStyle) -> Self {
        Self {
            verbs: Vec::new(),
            style: Style::Fill(style),
        }
    }

    /// Creates a filled path.
    pub fn new(fill: FillStyle, stroke: StrokeStyle) -> Self {
        Self {
            verbs: Vec::new(),
            style: Style::Colored { fill, stroke },
        }
    }

    /// Move the cursor to the position at `index`.
    pub fn move_to(&mut self, index: usize) {
        self.verbs.push(IndexedVerb::MoveTo(index));
    }

    /// Draw a line to the position at `index`.
    pub fn line_to(&mut self, index: usize) {
        self.verbs.push(IndexedVerb::LineTo(index));
    }

    /// Draw a quadratic curve along the position at `index1` to the position at `index2`.
    pub fn quad_to(&mut self, index1: usize, index2: usize) {
        self.verbs.push(IndexedVerb::QuadTo(index1, index2));
    }

    /// Draw a quadratic curve along the positions at `index1` and `index2` to the position at `index3`.
    pub fn bezier_to(&mut self, index1: usize, index2: usize, index3: usize) {
        self.verbs
            .push(IndexedVerb::BezierTo(index1, index2, index3));
    }

    /// Close the current path.
    pub fn close(&mut self) {
        self.verbs.push(IndexedVerb::Close);
    }

    /// Render the indexed path.
    pub fn render<R: Renderer>(&self, canvas: &mut Canvas<R>, points: &[Vector<f32>]) {
        let mut path = Path::new();
        for verb in &self.verbs {
            use IndexedVerb::*;
            match *verb {
                MoveTo(index) => {
                    let point = points[index];
                    path.move_to(point.x, point.y);
                }
                LineTo(index) => {
                    let point = points[index];
                    path.line_to(point.x, point.y);
                }
                QuadTo(index1, index2) => {
                    let point1 = points[index1];
                    let point2 = points[index2];
                    path.quad_to(point1.x, point1.y, point2.x, point2.y);
                }
                BezierTo(index1, index2, index3) => {
                    let point1 = points[index1];
                    let point2 = points[index2];
                    let point3 = points[index3];
                    path.bezier_to(point1.x, point1.y, point2.x, point2.y, point3.x, point3.y);
                }
                Close => path.close(),
            }
        }

        use Style::*;
        match &self.style {
            Stroke(style) => canvas.stroke_path(&path, &style.to_paint()),
            Fill(style) => canvas.fill_path(&path, &style.to_paint()),
            Colored { fill, stroke } => {
                canvas.fill_path(&path, &fill.to_paint());
                canvas.stroke_path(&path, &stroke.to_paint());
            }
        }
    }
}

impl<S: EndianSettings> ToStream<S> for IndexedPath {
    fn to_stream<W: Write>(&self, stream: &mut W) -> Result<()> {
        to_stream(&self.verbs, stream)?;
        to_stream::<S, _, _>(&self.style, stream)
    }
}

impl<S: EndianSettings> FromStream<S> for IndexedPath {
    fn from_stream<R: Read>(stream: &mut R) -> Result<Self> {
        Ok(Self {
            verbs: from_stream(stream)?,
            style: from_stream::<S, _, _>(stream)?,
        })
    }
}
