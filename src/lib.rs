#![deny(missing_docs)]

/*!
Structs to represent and render vector grapics based meshes.

The main mesh is represented by the `VectorMesh` struct.

They can be animated using the `Animation` struct.
*/

mod animation;
mod convert;
mod path;
mod style;

pub use animation::{Animation, AnimationFrame, AnimationState};
pub use path::IndexedPath;
pub use style::{FillStyle, StrokeStyle};

use data_stream::{FromStream, ToStream, collections::SizeSettings, from_stream, to_stream};
use femtovg::{Canvas, Renderer};
use ga2::Vector;
use touch_selection::Selector;
use vector_editor_core::{PointObject, SelectSettings};
use vector_space::distance;

use std::io::{Read, Result, Write};

/// A list consisting of multiple indexed paths to be rendered separately.
#[derive(Clone, Default)]
pub struct PathList {
    paths: Vec<IndexedPath>,
}

impl PathList {
    /// Create a new `PathList`.
    pub fn new() -> Self {
        Self { paths: Vec::new() }
    }
}

impl SelectSettings<Vector<f32>> for PathList {
    #[inline]
    fn grab_distance(&self) -> f32 {
        1.0
    }
}

impl<S: SizeSettings> ToStream<S> for PathList {
    fn to_stream<W: Write>(&self, stream: &mut W) -> Result<()> {
        to_stream::<S, _, _>(&self.paths, stream)
    }
}

impl<S: SizeSettings> FromStream<S> for PathList {
    fn from_stream<R: Read>(stream: &mut R) -> Result<Self> {
        Ok(Self {
            paths: from_stream::<S, _, _>(stream)?,
        })
    }
}

/// The type of the vector mesh.
///
/// It's a point object using vectors to represent the positions and specifying a list of paths as additional data.
pub type VectorMesh = PointObject<Vector<f32>, PathList>;

/// A trait for extension methods for the vector mesh.
pub trait VectorMeshExtensions: Sized {
    /// Sets the current shape by specifying an `animation` and its `state`.
    fn set_animation(&mut self, animation: &Animation, state: &AnimationState);

    /// Adds a new path to the mesh.
    fn add_path(&mut self, path: IndexedPath);

    /// Renders the mesh.
    fn render<R: Renderer>(&self, canvas: &mut Canvas<R>);
}

impl VectorMeshExtensions for VectorMesh {
    fn set_animation(&mut self, animation: &Animation, state: &AnimationState) {
        animation.set_shape(&mut self.points, state);
    }

    fn add_path(&mut self, path: IndexedPath) {
        self.options.paths.push(path);
    }

    fn render<R: Renderer>(&self, canvas: &mut Canvas<R>) {
        for indexed_path in &self.options.paths {
            indexed_path.render(canvas, &self.points);
        }
    }
}

/// A helper struct useful for editing vector meshes.
pub struct SelectionEditor {
    move_distance: f32,
    snapping: Option<i8>,
}

fn snap(pos: Vector<f32>, f: f32) -> Vector<f32> {
    let Vector { x, y } = pos;
    Vector::new((x / f).round(), (y / f).round()) * f
}

impl SelectionEditor {
    /// Creates a new `SelectionEditor`.
    pub fn new() -> Self {
        Self {
            move_distance: 0x10 as f32,
            snapping: Some(0),
        }
    }
}

impl Default for SelectionEditor {
    fn default() -> Self {
        Self::new()
    }
}

impl Selector for SelectionEditor {
    type Selectable = VectorMesh;
    type Position = Vector<f32>;

    fn moved(&self, moved: Vector<f32>, cursor: Vector<f32>) -> bool {
        distance(moved, cursor) >= self.move_distance
    }

    fn stop(&self, selectable: &mut Self::Selectable, index: usize) {
        if let Some(snapping) = self.snapping {
            let pos = &mut selectable.points[index];
            *pos = snap(*pos, 2.0f32.powi(snapping as i32));
        }
    }
}
