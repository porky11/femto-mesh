use crate::convert::Convert as _;

use data_stream::{FromStream, ToStream, from_stream, numbers::EndianSettings, to_stream};
use femtovg::{LineCap, LineJoin, Paint};
use simple_color::Color;

use std::io::{Error, ErrorKind, Read, Result, Write};

/// A style used for stroking.
#[derive(Clone)]
pub struct StrokeStyle {
    color: Color,
    line_width: f32,
    line_cap_start: LineCap,
    line_cap_end: LineCap,
    line_join: LineJoin,
}

impl Default for StrokeStyle {
    fn default() -> Self {
        Self {
            color: Color::BLACK,
            line_width: 1.0,
            line_cap_start: LineCap::Round,
            line_cap_end: LineCap::Round,
            line_join: LineJoin::Round,
        }
    }
}

impl StrokeStyle {
    /// Creates a stroke style by color.
    pub fn color(color: Color) -> Self {
        Self {
            color,
            ..Default::default()
        }
    }

    /// Changes the line width of the stroke style.
    #[inline]
    pub fn with_line_width(self, line_width: f32) -> Self {
        Self { line_width, ..self }
    }

    /// Changes the line cap of the stroke style.
    #[inline]
    pub fn with_line_cap(self, line_cap: LineCap) -> Self {
        let line_cap_start = line_cap;
        let line_cap_end = line_cap;
        Self {
            line_cap_start,
            line_cap_end,
            ..self
        }
    }

    /// Changes only the line cap start of the stroke style.
    #[inline]
    pub fn with_line_cap_start(self, line_cap_start: LineCap) -> Self {
        Self {
            line_cap_start,
            ..self
        }
    }

    /// Changes only the line cap end of the stroke style.
    #[inline]
    pub fn with_line_cap_end(self, line_cap_end: LineCap) -> Self {
        Self {
            line_cap_end,
            ..self
        }
    }

    /// Changes the line join of the stroke style.
    #[inline]
    pub fn with_line_join(self, line_join: LineJoin) -> Self {
        Self { line_join, ..self }
    }

    pub(crate) fn to_paint(&self) -> Paint {
        Paint::color(self.color.convert())
            .with_line_width(self.line_width)
            .with_line_cap_start(self.line_cap_start)
            .with_line_cap_end(self.line_cap_end)
            .with_line_join(self.line_join)
    }
}

/// A style used for filling.
#[derive(Clone, Default)]
pub struct FillStyle {
    color: Color,
}

impl FillStyle {
    /// Creates a fill style by color.
    pub fn color(color: Color) -> Self {
        Self { color }
    }

    pub(crate) fn to_paint(&self) -> Paint {
        Paint::color(self.color.convert())
    }
}

#[derive(Clone)]
pub enum Style {
    Stroke(StrokeStyle),
    Fill(FillStyle),
    Colored {
        fill: FillStyle,
        stroke: StrokeStyle,
    },
}

impl<S: EndianSettings> ToStream<S> for Style {
    fn to_stream<W: Write>(&self, stream: &mut W) -> Result<()> {
        use Style::*;
        match self {
            Stroke(style) => {
                to_stream::<S, _, _>(&0u8, stream)?;
                to_stream::<S, _, _>(&style.color, stream)?;
                to_stream::<S, _, _>(&style.line_width, stream)
            }
            Fill(style) => {
                to_stream::<S, _, _>(&1u8, stream)?;
                to_stream::<S, _, _>(&style.color, stream)
            }
            Colored { fill, stroke } => {
                to_stream::<S, _, _>(&2u8, stream)?;
                to_stream::<S, _, _>(&fill.color, stream)?;
                to_stream::<S, _, _>(&stroke.color, stream)?;
                to_stream::<S, _, _>(&stroke.line_width, stream)
            }
        }
    }
}

impl<S: EndianSettings> FromStream<S> for Style {
    fn from_stream<R: Read>(stream: &mut R) -> Result<Self> {
        let verb_kind: u8 = from_stream::<S, _, _>(stream)?;

        use Style::*;
        Ok(match verb_kind {
            0 => Stroke(StrokeStyle {
                color: from_stream::<S, _, _>(stream)?,
                line_width: from_stream::<S, _, _>(stream)?,
                ..Default::default()
            }),
            1 => Fill(FillStyle {
                color: from_stream::<S, _, _>(stream)?,
            }),
            2 => Colored {
                fill: FillStyle {
                    color: from_stream::<S, _, _>(stream)?,
                },
                stroke: StrokeStyle {
                    color: from_stream::<S, _, _>(stream)?,
                    line_width: from_stream::<S, _, _>(stream)?,
                    ..Default::default()
                },
            },
            _ => {
                return Err(Error::new(
                    ErrorKind::InvalidData,
                    "Trying to read invalid enum variant",
                ));
            }
        })
    }
}
